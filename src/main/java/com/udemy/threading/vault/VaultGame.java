package com.udemy.threading.vault;

import com.udemy.threading.vault.domain.AscendingHackerThread;
import com.udemy.threading.vault.domain.DescendingHackerThread;
import com.udemy.threading.vault.domain.PoliceThread;
import com.udemy.threading.vault.domain.Vault;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class VaultGame {
    public static void main(String[] args) throws InterruptedException {
        int maxGuesses = 9999;
        int password = new Random().nextInt(maxGuesses);
        Vault vault = new Vault(password);
        List<Thread> actors = new LinkedList<>();

        actors.add(new AscendingHackerThread(vault, maxGuesses));
        actors.add(new DescendingHackerThread(vault, maxGuesses));
        actors.add(new PoliceThread());

        actors.forEach(Thread::start);

        Thread.sleep(2500L);
    }
}
