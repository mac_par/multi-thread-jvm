package com.udemy.threading.vault.domain;

public class Vault {
    private final int password;

    public Vault(int password) {
        this.password = password;
    }

    public boolean isCorrectPassword(int guess) throws InterruptedException {
        Thread.sleep(5);
        return this.password == guess;
    }
}
