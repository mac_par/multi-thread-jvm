package com.udemy.threading.vault.domain;

public class AscendingHackerThread extends AbstractHacker {

    public AscendingHackerThread(Vault vault, int maxGuess) {
        super(vault, maxGuess);
    }

    @Override
    public void run() {
        for (int i = 0; i < maxGuess; i++) {
            try {
                if (vault.isCorrectPassword(i)) {
                    System.out.printf("Hacker %s hacked the vault with %d%n", this.getName(), i);
                    System.exit(0);
                }
            } catch (InterruptedException ex) {
                //empty on purpose
            }
        }
    }
}
