package com.udemy.threading.vault.domain;

public class DescendingHackerThread extends AbstractHacker {

    public DescendingHackerThread(Vault vault, int maxGuess) {
        super(vault, maxGuess);
    }

    @Override
    public void run() {
        for (int i = maxGuess; i >= 0; i--) {
            try {
                if (vault.isCorrectPassword(i)) {
                    System.out.printf("Hacker %s hacked the vault with %d%n", this.getName(), i);
                    System.exit(0);
                }
            } catch (InterruptedException ex) {
                //empty on purpose
            }
        }
    }
}
