package com.udemy.threading.vault.domain;

public abstract class AbstractHacker extends Thread {
    protected final Vault vault;
    protected final int maxGuess;

    protected AbstractHacker(Vault vault, int maxGuess) {
        super();
        this.vault = vault;
        this.maxGuess = maxGuess;
        init();
    }

    protected void init() {
        this.setName(this.getClass().getSimpleName());
        this.setPriority(MAX_PRIORITY);
    }

    @Override
    public synchronized void start() {
        System.out.printf("Starting: %s%n", this.getName());
        super.start();
    }
}
