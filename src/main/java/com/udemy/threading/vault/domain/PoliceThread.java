package com.udemy.threading.vault.domain;

public class PoliceThread extends Thread {
    public PoliceThread() {
        super(PoliceThread.class.getSimpleName());
    }

    @Override
    public void run() {
        for (int i = 10; i >= 0; i--) {
            try {
                Thread.sleep(1000L);
            } catch(InterruptedException ex) {
                //empty on purpose
            }
            System.out.printf("Police: %d%n",i);
        }
        System.out.println("Police stops the execution");
        System.exit(0);
    }
}
