package com.udemy.threading.creation1;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Starting Main");
        Thread th = new Thread(() -> {
            System.out.printf("Running from thread: %s, priority: %d%n", Thread.currentThread().getName(), Thread.currentThread().getPriority());
        });

        th.setName("Boski-thread");
        th.setPriority(Thread.MAX_PRIORITY - 2);

        System.out.println("Starting th");
        th.start();
        System.out.println("Starting th");

        Thread.sleep(1000L);
    }
}
