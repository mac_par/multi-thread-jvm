package com.udemy.threading.creation1;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

public class FactorialsCount {
    public static final void main(String[] args) throws InterruptedException {
        List<Long> numbs = List.of(10000000000L, 0L, 3435L, 35435L, 2324L, 4656L, 23L, 2435L, 5566L);

        List<FunkyFuct> collect = numbs.stream().map(FunkyFuct::new).collect(Collectors.toList());
        collect.forEach(FunkyFuct::start);
        for (FunkyFuct funkyFuct : collect) {
            funkyFuct.join(2000);
        }
        collect.forEach(item -> {
            if (item.isFinished()) {
                System.out.printf("Calculation finished for %d - %s%n", item.getInputNumber(), item.getResult());
            } else {
                System.out.printf("Still calculating for %d%n", item.getInputNumber());
            }
        });
    }

    private static class FunkyFuct extends Thread {
        private BigInteger result;
        private final long inputNumber;
        private boolean isFinished;

        public FunkyFuct(long inputNumber) {
            this.inputNumber = inputNumber;
        }

        @Override
        public void run() {
            result = functorial();
            isFinished = true;
        }

        private BigInteger functorial() {
            BigInteger value = BigInteger.ONE;

            for (BigInteger i = new BigInteger(String.valueOf(inputNumber)); i.compareTo(BigInteger.ONE) > 0; i = i.subtract(BigInteger.ONE)) {
                value = value.multiply(i);
               /* try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    //empty on purpose
                }*/
            }
            return value;
        }

        public BigInteger getResult() {
            return result;
        }

        public long getInputNumber() {
            return inputNumber;
        }

        public boolean isFinished() {
            return isFinished;
        }
    }
}
