package com.udemy.threading.atomaticity.case1;

public class Metrics {
    private int count;
    private volatile double average;

    public Metrics() {
    }

    public synchronized void addSample(long sampleValue) {
        double currentSum = average * count++;
        average = (currentSum + sampleValue) / count;
    }

    public double getAverage() {
        return average;
    }
}
