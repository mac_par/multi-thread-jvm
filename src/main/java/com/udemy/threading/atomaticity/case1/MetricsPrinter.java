package com.udemy.threading.atomaticity.case1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MetricsPrinter extends Thread {
    private final Metrics metrics;

    public MetricsPrinter(Metrics metrics) {
        this.metrics = metrics;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                //empty on purpose
            }
            log.info("average: {}", metrics.getAverage());
        }
    }
}
