package com.udemy.threading.atomaticity.case1;

import java.util.concurrent.ThreadLocalRandom;

public class BusinessLogic extends Thread {
    private final Metrics metrics;
    private ThreadLocalRandom localRandom = ThreadLocalRandom.current();

    public BusinessLogic(Metrics metrics) {
        this.metrics = metrics;
    }

    @Override
    public void run() {
        while (true) {
            long start = System.currentTimeMillis();
            try {
                Thread.sleep(localRandom.nextInt(10));
            } catch (InterruptedException ex) {
                //empty on purpose
            }
            long end = System.currentTimeMillis();
            metrics.addSample(end - start);
        }
    }
}
