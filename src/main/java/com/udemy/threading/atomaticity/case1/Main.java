package com.udemy.threading.atomaticity.case1;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Metrics metrics = new Metrics();

        BusinessLogic logic1 = new BusinessLogic(metrics);
        BusinessLogic logic2 = new BusinessLogic(metrics);
        MetricsPrinter printer = new MetricsPrinter(metrics);
        logic1.start();
        logic2.start();
        printer.start();

        Thread.sleep(2000);
    }
}
