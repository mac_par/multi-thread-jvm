package com.udemy.threading.atomaticity.case2;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MinMaxMetrics {
    private volatile long min = Long.MAX_VALUE;
    private volatile long max = Long.MIN_VALUE;

    public MinMaxMetrics() {
    }

    public void addSample(long sample) {
        log.info("Sample value: {}", sample);
        if (sample < min) {
            min = sample;
        }

        if (sample > max) {
            max = sample;
        }
    }

    public long getMin() {
        return min;
    }

    public long getMax() {
        return max;
    }
}
