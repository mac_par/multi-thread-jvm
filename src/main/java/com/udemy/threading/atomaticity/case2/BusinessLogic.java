package com.udemy.threading.atomaticity.case2;

import java.util.concurrent.ThreadLocalRandom;

public class BusinessLogic extends Thread {
    private final MinMaxMetrics metrics;
    private final ThreadLocalRandom localRandom = ThreadLocalRandom.current();

    public BusinessLogic(MinMaxMetrics metrics) {
        this.metrics = metrics;
    }

    @Override
    public void run() {
        while(true) {
            long start = System.currentTimeMillis();
            try {
                Thread.sleep(localRandom.nextInt(22));
            } catch (InterruptedException ex) {
                // empty on purpose
            }
            long end = System.currentTimeMillis();
            metrics.addSample(end - start);
        }
    }
}
