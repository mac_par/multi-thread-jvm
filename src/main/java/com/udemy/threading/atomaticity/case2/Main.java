package com.udemy.threading.atomaticity.case2;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        MinMaxMetrics metrics = new MinMaxMetrics();
        BusinessLogic businessLogic1 = new BusinessLogic(metrics);
        BusinessLogic businessLogic2 = new BusinessLogic(metrics);
        MetricsPrinter metricsPrinter = new MetricsPrinter(metrics);
        businessLogic1.start();
        businessLogic2.start();
        metricsPrinter.start();
        Thread.sleep(2500);
    }
}
