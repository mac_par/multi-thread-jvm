package com.udemy.threading.atomaticity.case2;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MetricsPrinter extends Thread {
    private final MinMaxMetrics metrics;

    public MetricsPrinter(MinMaxMetrics metrics) {
        this.metrics = metrics;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                //empty on purpose
            }
            log.info("Min: {}, Max: {}", metrics.getMin(), metrics.getMax());
        }
    }
}
