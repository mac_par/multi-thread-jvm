package com.udemy.threading.race;

public class IncrementingThread extends Thread {
    private final int rounds;
    private final Counter counter;

    public IncrementingThread(int rounds, Counter counter) {
        this.rounds = rounds;
        this.counter = counter;
    }

    @Override
    public void run() {
        for (int i = 0; i < rounds; i++) {
            counter.increment();
        }
    }
}
