package com.udemy.threading.race;

public class DecrementingThread extends Thread {
    private final int rounds;
    private final Counter counter;

    public DecrementingThread(int rounds, Counter counter) {
        this.rounds = rounds;
        this.counter = counter;
    }

    @Override
    public void run() {
        for (int i = rounds; i > 0; i--) {
            counter.decrement();
        }
    }
}
