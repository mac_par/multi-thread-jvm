package com.udemy.threading.race.data;

public class DataRaceMain {
    public static void main(String[] args) throws InterruptedException {
        int marrat = Integer.MAX_VALUE;
        Racen racen = new Racen();

        for (int i = 0; i < marrat; i++) {
            new Thread(() -> racen.increment()).start();
            new Thread(() -> racen.check()).start();
        }

        Thread.sleep(2500);
    }
}
