package com.udemy.threading.race.data;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Racen {
    private final Lock lock = new ReentrantLock();
    private volatile int x;
    private volatile int y;

    public void increment() {
        try {
            lock.lock();
            x++;
            y++;
        } catch (Exception thx) {
            //empty on purpose
        } finally {
            lock.unlock();
        }
    }

    public void check() {
        if (y > x) {
            System.out.println("Data race");
        }
    }
}
