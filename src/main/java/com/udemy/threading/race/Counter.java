package com.udemy.threading.race;

public class Counter {
    private int counter = 0;

    public Counter() {
    }

    public int getCounter() {
        return counter;
    }

    public void increment() {
        this.counter++;
    }

    public void decrement() {
        this.counter--;
    }
}
