package com.udemy.threading.race;

public class RaceConditionMain {
    public static void main(String[] args) throws InterruptedException {
        int rounds = 10_000;
        Counter counter = new Counter();
        IncrementingThread incr = new IncrementingThread(rounds, counter);
        DecrementingThread decr = new DecrementingThread(rounds, counter);

        incr.start();
        decr.start();
        incr.join();
        decr.join();
        System.out.println(counter.getCounter());
    }
}
