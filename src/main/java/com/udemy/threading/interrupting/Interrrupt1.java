package com.udemy.threading.interrupting;

import java.math.BigInteger;

public class Interrrupt1 {
    public static void main(String[] args) throws InterruptedException {
        Thread th = new Thread(new PowerClass(BigInteger.TWO,new BigInteger("1564")));

        th.start();
        th.interrupt();
        System.out.println("Stop");
    }

    private static class PowerClass implements Runnable {
        private final BigInteger base;
        private final BigInteger power;

        public PowerClass(BigInteger base, BigInteger power) {
            this.base = base;
            this.power = power;
        }

        @Override
        public void run() {
           System.out.printf("%s ^ %s = %s\n", base, power, calculatePower());
        }

        private BigInteger calculatePower() {
            BigInteger result = BigInteger.ONE;
            BigInteger incrementBy = BigInteger.ONE;

            for (BigInteger i = BigInteger.ZERO; i.compareTo(power) < 0; i = i.add(incrementBy)) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("Is interrupted");
                    return BigInteger.ZERO;
                }
                result = result.multiply(base);
            }

            return result;
        }
    }
}
