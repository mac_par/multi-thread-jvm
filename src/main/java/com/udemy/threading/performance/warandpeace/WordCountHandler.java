package com.udemy.threading.performance.warandpeace;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class WordCountHandler implements HttpHandler {
    private static final String QUERY_KEY = "word";
    private static final String QUERY_SEPARATOR = "=";
    private final String textContent;

    public WordCountHandler(String textContent) {
        this.textContent = textContent;
    }

    //search?word=***
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String queryString = exchange.getRequestURI().getQuery();
        String word = extractQueryValue(queryString, QUERY_KEY);
        if (isBlank(word)) {
            exchange.sendResponseHeaders(400, 0);
        } else {
            long count = countWords(word);
            byte[] responseBody = Long.toString(count).getBytes();
            exchange.sendResponseHeaders(200, responseBody.length);
            try (OutputStream outputStream = exchange.getResponseBody()) {
                outputStream.write(responseBody);
            }
        }
    }

    private long countWords(String word) {
        long count = 0;
        int index = 0;

        do  {
            index = textContent.indexOf(word, index);
            if (indexWasFound(index)) {
                index++;
                count++;
            }
        } while (indexWasFound(index));
        return count;
    }

    private boolean indexWasFound(int index) {
        return index >= 0;
    }

    static String extractQueryValue(String query, String key) {
        if (isBlank(query) || !query.contains(key + QUERY_SEPARATOR)) {
            return "";
        }

        String[] values = query.split("&");
        String queryKey = Stream.of(values).filter(val -> val.startsWith(key)).findFirst().get();
        return queryKey.substring(queryKey.indexOf(QUERY_SEPARATOR) + 1);
    }
}
