package com.udemy.threading.performance.warandpeace;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class HServer {
    private static final String WAR_AND_PEACE_BOOK = "war_and_peace.txt";
    private static final int THREAD_COUNT = 1;
    private static final Executor POOL = Executors.newFixedThreadPool(THREAD_COUNT);

    public static void main(String[] args) throws URISyntaxException, IOException {
        String text = new String(Files.readAllBytes(getFilePath(WAR_AND_PEACE_BOOK)));
        startServer(text);
        System.out.println("Server is running!");
    }

    private static void startServer(String text) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/search", new WordCountHandler(text));
        server.setExecutor(POOL);
        server.start();
    }

    private static Path getFilePath(String filePath) throws URISyntaxException {
        return Paths.get(HServer.class.getClassLoader().getResource(filePath).toURI());
    }
}
