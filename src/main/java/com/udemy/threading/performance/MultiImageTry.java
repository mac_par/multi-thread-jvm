package com.udemy.threading.performance;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class MultiImageTry {
    private static final String FILE = "many-flowers.jpg";
    private static final String OUT_FILE = "./out/many-flowers.jpg";

    public static void main(String[] args) throws IOException {
        BufferedImage source = ImageIO.read(Objects.requireNonNull(MultiImageTry.class.getClassLoader().getResource(FILE)));
        ImageProcessingMultiThread imageProcessing = new ImageProcessingMultiThread();
        long start = System.currentTimeMillis();
        BufferedImage recoloredImage = imageProcessing.recolorImage(source, 4);
        long end = System.currentTimeMillis();
        ImageIO.write(recoloredImage, "jpg", new File(OUT_FILE));
        System.out.println(end - start);
    }
}
