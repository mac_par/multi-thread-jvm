package com.udemy.threading.performance;

import java.awt.image.BufferedImage;

public class ImageProcessing {

    public boolean isShade(int red, int blue, int green) {
        return Math.abs(red - green) < 30 && Math.abs(red - blue) < 30 && Math.abs(green - blue) < 30;
    }

    public int createRGBA(int red, int green, int blue) {
        int rgba = 0;
        rgba |= blue;
        rgba |= green << 8;
        rgba |= red << 16;
        rgba |= 0xFF000000;
        return rgba;
    }

    public int getRed(int rgb) {
        return (rgb & 0x00FF0000) >> 16;
    }

    public int getGreen(int rgb) {
        return (rgb & 0x0000FF00) >> 8;
    }

    public int getBlue(int rgb) {
        return rgb & 0x000000FF;
    }

    private void processImage(BufferedImage sourceImage, BufferedImage destImage, int x, int y) {
        int rgb = sourceImage.getRGB(x, y);
        int red = getRed(rgb);
        int green = getGreen(rgb);
        int blue = getBlue(rgb);

        if (isShade(red, blue, green)) {
            red = Math.min(255, red + 10);
            green = Math.max(0, green - 80);
            blue = Math.max(0, blue - 20);
        }

        destImage.getRaster().setDataElements(x, y,
                destImage.getColorModel().getDataElements(createRGBA(red, green, blue), null));
    }

    public BufferedImage recolorImage(BufferedImage sourceImage) {
        BufferedImage destImage = new BufferedImage(sourceImage.getWidth(), sourceImage.getHeight(), BufferedImage.TYPE_INT_RGB);
        recolorSingleThread(sourceImage, destImage);
        return destImage;
    }

    public void recolorSingleThread(BufferedImage src, BufferedImage dest) {
        int height = src.getHeight();
        int width = src.getWidth();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                processImage(src, dest, x, y);
            }
        }
    }
}
