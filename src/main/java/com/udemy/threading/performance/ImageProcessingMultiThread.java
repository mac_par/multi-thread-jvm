package com.udemy.threading.performance;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

public class ImageProcessingMultiThread {

    public boolean isShade(int red, int blue, int green) {
        return Math.abs(red - green) < 30 && Math.abs(red - blue) < 30 && Math.abs(green - blue) < 30;
    }

    public int createRGBA(int red, int green, int blue) {
        int rgba = 0;
        rgba |= blue;
        rgba |= green << 8;
        rgba |= red << 16;
        rgba |= 0xFF000000;
        return rgba;
    }

    public int getRed(int rgb) {
        return (rgb & 0x00FF0000) >> 16;
    }

    public int getGreen(int rgb) {
        return (rgb & 0x0000FF00) >> 8;
    }

    public int getBlue(int rgb) {
        return rgb & 0x000000FF;
    }

    private void processImage(BufferedImage sourceImage, BufferedImage destImage, int x, int y) {
        int rgb = sourceImage.getRGB(x, y);
        int red = getRed(rgb);
        int green = getGreen(rgb);
        int blue = getBlue(rgb);

        if (isShade(red, blue, green)) {
            red = Math.min(255, red + 10);
            green = Math.max(0, green - 80);
            blue = Math.max(0, blue - 20);
        }

        destImage.getRaster().setDataElements(x, y,
                destImage.getColorModel().getDataElements(createRGBA(red, green, blue), null));
    }

    public BufferedImage recolorImage(BufferedImage sourceImage, int cores) {
        BufferedImage destImage = new BufferedImage(sourceImage.getWidth(), sourceImage.getHeight(), BufferedImage.TYPE_INT_RGB);
        List<Thread> processingThread = new LinkedList<>();
        int height = sourceImage.getHeight() / cores;
        int width = sourceImage.getWidth();
        int pointX = 0;
        for (int i = 0; i < cores; i++) {
            int pointY = i * height;
            Thread thread = new Thread(() -> recolor(sourceImage, destImage, pointX, pointY, width, height));
            processingThread.add(thread);
        }

        processingThread.forEach(Thread::start);

        for (Thread thread: processingThread) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return destImage;
    }

    public void recolor(BufferedImage src, BufferedImage dest, int pointX, int pointY, int chunkWidth, int chunkHeight) {
        for (int x = pointX; x < chunkWidth; x++) {
            for (int y = pointY; y < (pointY + chunkHeight); y++) {
                processImage(src, dest, x, y);
            }
        }
    }
}
